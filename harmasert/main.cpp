#include <iostream>
#include <sstream>
#include "../library/stringstreamenumerator.hpp"
#include "../library/maxsearch.hpp"
#include "../library/linsearch.hpp"
#include "../library/seqinfileenumerator.hpp"


struct Fogas
{
    std::string hal;
    int suly;

    friend std::istream& operator>>(std::istream& is, Fogas& f)
    {
        is >> f.hal >> f.suly;
        return is;
    }

};

class MaxFogasSearch : public MaxSearch<Fogas, int, Greater<int>>
{
    int func(const Fogas& e) const
    {
        return e.suly;
    }

};

struct Horgasz
{
    std::string nev;
    std::string verseny;
    bool legnagyobbKeszegVolt;

    friend std::istream& operator>>(std::istream& is, Horgasz& h)
    {
        std::string line;
        std::getline(is, line);
        std::stringstream stream(line);
        stream >> h.nev >> h.verseny;

        StringStreamEnumerator<Fogas> sse(stream);
        MaxFogasSearch maxFogas;
        maxFogas.addEnumerator(&sse);
        maxFogas.run();

        h.legnagyobbKeszegVolt = maxFogas.found() && maxFogas.optElem().hal == "Keszeg";

        return is;
    }

};


class LegnagyobbKeszegVoltSearch : public LinSearch<Horgasz>
{
    bool cond(const Horgasz& e) const override
    {
        return e.legnagyobbKeszegVolt;
    }
};

int main()
{
    try {
        SeqInFileEnumerator<Horgasz> t("../input.txt");

        LegnagyobbKeszegVoltSearch s;
        s.addEnumerator(&t);
        s.run();

        if (s.found()) {
            std::cout << "Egy olyan sor, ahol a legnagyobb fogas keszeg volt: " << s.elem().nev << " " << s.elem().verseny << std::endl;
        } else {
            std::cout << "Nem volt olyan sor, amelyben a legnagyobb fogas keszeg lett volna!" << std::endl;
        }

    } catch (SeqInFileEnumerator<Horgasz>::Exceptions) {
        std::cerr << "A megadott fajlt nem sikerult megnyitni!" << std::endl;
    }
    return 0;
}
