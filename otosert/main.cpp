#include <iostream>
#include <sstream>
#include "../library/stringstreamenumerator.hpp"
#include "../library/maxsearch.hpp"
#include "../library/seqinfileenumerator.hpp"
#include "../library/enumerator.hpp"
#include "../library/summation.hpp"


struct Fogas
{
    std::string hal;
    int suly;

    friend std::istream& operator>>(std::istream& is, Fogas& f)
    {
        is >> f.hal >> f.suly;
        return is;
    }

};

class MaxFogasSearch : public MaxSearch<Fogas, int, Greater<int>>
{
    int func(const Fogas& e) const
    {
        return e.suly;
    }

};

struct Horgasz
{
    std::string nev;
    std::string verseny;
    bool legnagyobbKeszegVolt;

    friend std::istream& operator>>(std::istream& is, Horgasz& h)
    {
        std::string line;
        std::getline(is, line);
        std::stringstream stream(line);
        stream >> h.nev >> h.verseny;

        StringStreamEnumerator<Fogas> sse(stream);
        MaxFogasSearch maxFogas;
        maxFogas.addEnumerator(&sse);
        maxFogas.run();

        h.legnagyobbKeszegVolt = maxFogas.found() && maxFogas.optElem().hal == "Keszeg";

        return is;
    }

};


struct Verseny
{
    std::string nev;
    bool mindigKeszegLegnagyobb;

};

class LogicalSummation : public Summation<Horgasz, bool>
{
    public:
        LogicalSummation(std::string verseny): _verseny(verseny) { }

    private:
        bool neutral() const override { return true; }
        bool add(const bool& a, const bool& b) const override
        {
            return a && b;
        }
        bool func(const Horgasz& e) const override
        {
            return e.legnagyobbKeszegVolt;
        }
        bool whileCond(const Horgasz& current) const override { return current.verseny == _verseny; }
        void first() override {}
        std::string _verseny;
};


class VersenyEnumerator : public Enumerator<Verseny>
{
    public:
        VersenyEnumerator(const std::string& filename): _t(filename) { _end = false; }
        bool end() const override { return _end; }
        Verseny current() const override { return _curr; }
        void first() override { _t.first(); next(); }

        void next() override
        {
            if (_t.end()) {
                _end = true;
                return;
            }

            _curr.nev = _t.current().verseny;

            LogicalSummation l(_curr.nev);
            l.addEnumerator(&_t);
            l.run();

            _curr.mindigKeszegLegnagyobb = l.result();

        }

    private:
        bool _end;
        Verseny _curr;
        SeqInFileEnumerator<Horgasz> _t;
};

class PrintScreenSummation : public Summation<Verseny, std::ostream>
{
    public:
        PrintScreenSummation(std::ostream* o): Summation<Verseny, std::ostream>(o) { }
    private:
        bool  cond(const Verseny &item) const override
        {
            return item.mindigKeszegLegnagyobb;
        }

        std::string func(const Verseny &item) const override
        {
            return item.nev + "\n";
        }
};


int main()
{
    try {

        VersenyEnumerator t("../input.txt");
        PrintScreenSummation printScreen(&std::cout);
        printScreen.addEnumerator(&t);

        std::cout << "Azon versenyek, ahol a legnagyobb fogas mindig keszeg volt:" << std::endl;
        printScreen.run();

    } catch (SeqInFileEnumerator<Horgasz>::Exceptions) {
        std::cerr << "A megadott fajlt nem sikerult megnyitni!" << std::endl;
    }
    return 0;
}
